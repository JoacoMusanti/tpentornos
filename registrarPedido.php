<?php
    if(!isset($_SESSION)) {
        session_start();
    }

    if (!isset($_SESSION["Usuario"]) || !isset($_SESSION["Carro"]))
    {
        echo '<script type="text/javascript">
                        window.location = "index.php"
                    </script>';

        return;
    }

    include ("conexion.inc");

    $hoy = date("Y-m-d");
    $ahora = date("H:i:s");
    $idUsuario = $_SESSION["Usuario"]['id'];
    $carro = $_SESSION["Carro"];

    $query = "INSERT INTO pedidos (fecha, hora, id_usuario) VALUES ('$hoy', '$ahora', $idUsuario)";
    mysqli_query($link, $query) or die (mysqli_error($link));
    $id = mysqli_insert_id($link);
    $cadena = "";

    for ($i = 0; $i < count($carro); $i++)
    {
        $cantidad = $carro[$i]["cantidad"];
        $idComida = $carro[$i]["id"];

        $query = "INSERT INTO comida_pedido (id_comida, id_pedido, cantidad) VALUES ('$idComida', '$id', '$cantidad')";
        mysqli_query($link, $query) or die (mysqli_error($link));

        $query = "Select * from comidas where id ='$idComida'";
        $resultado = mysqli_query($link, $query) or die (mysqli_error($link));
        $row = mysqli_fetch_assoc($resultado);
        $comida = $row["nombre"];
        $restaurante = $row["restaurante"];
        $dirrestaurante = $row["direccionRestaurante"];
        $total= $cantidad* $row["precio"];
        $nrocomida = $i+1;
        $cadenai = "\r\n Nombre de la comida Nº$nrocomida: $comida\r\n Cantidad pedida: $cantidad \r\n Nombre del restaurante:$restaurante \r\n Direccion del Restaurante: $dirrestaurante \r\n Total: $$total\r\n";
        $cadena=$cadena.$cadenai;
    }


    mysqli_close($link);

     $email = $_SESSION["Usuario"]["email"];
     $nombre = $_SESSION["Usuario"]["nombre"];


     $mensaje = "Hola $nombre, tu pedido se cargó correctamente. Tu número de pedido es $id y contiene lo siguiente: \r\n $cadena \r\n Gracias por utilizar BonAppetitYa!";

     $to=$email;
     $subject="Pedido cargado exitosamente";
     mail($to,$subject,$mensaje);

    echo '<script type="text/javascript">
                        window.location = "pedidoCargado.php"
                    </script>';
    return;
?>