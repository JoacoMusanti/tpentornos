<?php if(!isset($_SESSION)) {
    session_start();
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bon AppetitYA &mdash; Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="js/jquery.waypoints.min.js"></script>

    <!-- Magnific -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>
    <!-- Isotope & imagesLoaded -->
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <!-- GSAP  -->
    <script src="js/TweenLite.min.js"></script>
    <script src="js/CSSPlugin.min.js"></script>
    <script src="js/EasePack.min.js"></script>

    <!-- MAIN JS -->
    <script src="js/main.js"></script>

    <script type="text/javascript">


        $(document).ready(function () {
            $.ajax({
                data: {ejecutar: 'obtenerItemsCarro'},
                method: "post",
                url: "funcionesAuxiliares.php",
                success: function (respuesta) {
                    $("#cuerpoCarrito").html(respuesta);
                }
            });

            $.ajax({
                data: {ejecutar: 'obtenerCantidadItemsCarro'},
                method: "post",
                url: "funcionesAuxiliares.php",
                success: function (respuesta) {
                    if (respuesta != 0)
                    {
                        $("#btnComprar").attr("disabled", false);
                    }
                    else
                    {
                        $("#btnComprar").attr("disabled", true);
                    }
                }
            });

            $.ajax({
                data: {ejecutar: 'calcularTotalCarro'},
                method: "post",
                url: "funcionesAuxiliares.php",
                success: function (respuesta) {
                    $("#precio").html(respuesta);
                }
            });

            $("#carrito").on("change", ".elemento-carrito", function () {
                var elemento = {id: $(this).attr("name"), cantidad: $(this).val()};

                if ($(this).val() > 0)
                {
                    $.ajax({
                        type: "post",
                        url: "actualizarCarro.php",
                        data: {pedido: elemento},
                        success: function(rta){
                            $("#precio").html(rta);
                        }
                    });
                }
                else
                {
                    $(this).val(1);
                }
            });

            $("#carrito").on("click", ".btnEliminar", function () {
                var id = $(this).attr("name");

                $.ajax({
                    data: { id: id },
                    method: "post",
                    url: "eliminarDelCarro.php",
                    success: function (respuesta) {
                        $.ajax({
                            data: {ejecutar: 'obtenerItemsCarro'},
                            method: "post",
                            url: "funcionesAuxiliares.php",
                            success: function (respuesta) {
                                $("#cuerpoCarrito").html(respuesta);
                            }
                        });

                        $.ajax({
                            data: {ejecutar: 'calcularTotalCarro'},
                            method: "post",
                            url: "funcionesAuxiliares.php",
                            success: function (respuesta) {
                                $("#precio").html(respuesta);

                                if (respuesta != 0)
                                {
                                    $("#btnComprar").attr("disabled", false);
                                }
                                else
                                {
                                    $("#btnComprar").attr("disabled", true);
                                }
                            }
                        });
                    }
                });
            });

            $("#btnBuscar").click(function () {
                $.ajax({
                    data: {busqueda: $("#txtBusqueda").val()},
                    method: "POST",
                    url: "buscar.php",
                    success: function (devolucion) {
                        $("#lista-comida").html(devolucion);
                    }
                });
            });

            $('#logOut').click(function()
            {
                var submit = "Salir";
                $.ajax({
                    type: "post",
                    url: "login.php",
                    data: {submit: submit},
                    success: function(){
                        window.location = "index.php";
                    }
                });
            });
        })
    </script>
</head>

<body>
    <nav class="navbar navbar-default" >
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="index.php" class="link-logo"><img class="img-responsive imagen-logo" src="images/LOGO.jpg" alt="HOME"></a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a class="navbar-btn" href="contacto.php">Ayuda y Contacto</a> </li>
                <li><a class="navbar-btn" href="nosotros.php">Nosotros</a></li>

                <?php

                if (isset($_SESSION["Admin"]))
                {
                    echo '<li><a class="navbar-btn" href="listarComidas.php">Listar Comidas</a></li>';
                    echo '<li><a class="navbar-btn" href="cargarComida.php">Cargar Comida</a></li>';
                }
                if (isset($_SESSION["Usuario"]))
                {
                    ?>
                    <li><a class="navbar-btn" href="editarUsuario.php"><?php echo $_SESSION["Usuario"]["usuario"] ?></a></li>
                    <li><a class="navbar-btn" href="#" id="logOut"><span class="fa fa-sign-out" aria-hidden="true"></span>Salir</a></li>
                    <?php
                }
                else
                {
                    ?>
                    <li><a href="cargarUsuario.php" class="navbar-btn">Registrarse</a> </li>
                    <li><a href="formularioLogin.php" class="navbar-btn"> <span class="fa fa-sign-in" aria-hidden="true"></span>Entrar</a></li>
                    <?php
                } ?>
            </ul>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <img class="img-responsive imagen-logo center-block" src="images/Banner.jpg" alt="Pedi lo que vas a comer hoy">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="buscador center-block">
                    <label for="txtBusqueda">Buscar</label>

                    <div class="input-group">
                        <input id="txtBusqueda" type="text" class="form-control" placeholder="Ingrese comida" />
                        <span class="input-group-btn">
                            <button id="btnBuscar" class="btn btn-default" type="submit">Buscar</button>
                       </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="container-fluid">
        <div id="carrito" class="col-sm-3">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-info">
                        <div id="cuerpoCarrito" class="panel-body">


                        </div>
                        <div class="panel-footer">
                            <div class="row text-center">
                                <div class="col-sm-4">
                                    <h4 class="text-right">Total <strong>$<span id="precio"></span> </strong></h4>
                                </div>
                                <div class="col-sm-8">
                                    <?php
                                    if (!isset($_SESSION["Usuario"]))
                                    {
                                        echo '<p>Debe loguearse para realizar una compra</p>';
                                    }
                                    else
                                    {
                                        ?>
                                        <form action="confirmarPedido.php" method="post">
                                            <input id="btnComprar" type="submit" value="Comprar" class="btn btn-success btn-block">
                                        </form>
                                        <?php
                                    }
                                    ?>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9">
            <div id="lista-comida" class="contenedor-comida">
                <div class="row">
                <?php
                include("conexion.inc");

                $cantPagina = 5;

                if (isset($_GET["pagina"]))
                {
                    $pagina = $_GET["pagina"];
                    $inicial = ($pagina - 1) * $cantPagina;
                }
                else
                {
                    $inicial = 0;
                }

                $query = "SELECT * FROM comidas WHERE baja <> true ORDER BY evaluacion DESC LIMIT $inicial,$cantPagina";
                $resultado = mysqli_query($link, $query) or die (mysqli_error($link));

                $html = "<a href=\"comida.php?id=*ID*\"><div class=\"card card-small\"><img src=\"*DIRECCION*\" class=\"img-responsive thumbnail thumbnail-sin-padding\" alt=\"*RESTAURANTE*\"><div class=\"container-card\"><div class='texto-corto'> <h4>*NOMBRE*</h4></div>*ESTRELLAS*<p class='texto-corto'>*RESTAURANTE*</p><h4 class=\"pull-right\">$*PRECIO*</h4></div></div></a>";
                $estrellas = "<div class=\"star-rating\"><div class=\"star-rating__wrap\"><span class=\"star-readonly fa *TIPO-ESTRELLA-5* fa-lg\" title=\"5 out of 5 stars\"></span><span class=\"star-readonly fa *TIPO-ESTRELLA-4* fa-lg\" title=\"4 out of 5 stars\"></span><span class=\"star-readonly fa *TIPO-ESTRELLA-3* fa-lg\" title=\"3 out of 5 stars\"></span><span class=\"star-readonly fa *TIPO-ESTRELLA-2* fa-lg\" title=\"2 out of 5 stars\"></span><span class=\"star-readonly fa *TIPO-ESTRELLA-1* fa-lg\" title=\"1 out of 5 stars\"></span></div></div>";
                $actual = "";
                $actualEstrellas = "";
                $devolucion = "";
                $estrellaLlena = "fa-star";
                $estrellaVacia = "fa-star-o";

                while ($row = mysqli_fetch_array($resultado))
                {
                $puntaje = round($row['evaluacion']);

                $actual = str_replace("*ID*", $row['id'], $html);
                $actual = str_replace("*DIRECCION*", $row['direccionImagePortada'], $actual);
                $actual = str_replace("*NOMBRE*", $row['nombre'], $actual);
                $actual = str_replace("*PRECIO*", $row['precio'], $actual);
                $actual = str_replace("*RESTAURANTE*", $row['restaurante'], $actual);

                if ($puntaje == 5)
                {
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaLlena, $estrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaLlena, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaLlena, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaLlena, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
                }
                elseif ($puntaje == 4)
                {
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaLlena, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaLlena, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaLlena, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
                }
                elseif ($puntaje == 3)
                {
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaVacia, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaLlena, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaLlena, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
                }
                elseif ($puntaje == 2)
                {
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaVacia, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaVacia, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaLlena, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
                }
                elseif ($puntaje == 1)
                {
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaVacia, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaVacia, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaVacia, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
                }
                else
                {
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaVacia, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaVacia, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaVacia, $actualEstrellas);
                $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaVacia, $actualEstrellas);
                }

                $actual = str_replace("*ESTRELLAS*", $actualEstrellas, $actual);


                $devolucion = $devolucion . $actual;
                }

                mysqli_close($link);

                echo $devolucion;
                ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="text-center">
                <?php

                include("conexion.inc");

                $query = "SELECT COUNT(*) FROM comidas WHERE baja <> true";
                $resultado = mysqli_query($link, $query) or die (mysqli_error($link));
                $cantidadComidas = mysqli_fetch_array($resultado);
                $nroDePaginas = ceil($cantidadComidas[0] / $cantPagina);

                for ($i = 0; $i < $nroDePaginas; $i++)
                {
                    $pag = $i+1;
                    if (isset($pagina))
                    {
                        if ($pagina == $pag)
                        {
                            echo $pag . "&nbsp;&nbsp;";
                        }
                        else
                        {
                            echo '<a href="index.php?pagina='.$pag.'">'.$pag.'</a>&nbsp;&nbsp;&nbsp;';
                        }
                    }
                    else
                    {
                        if ($pag == 1)
                        {
                            echo $pag . "&nbsp;&nbsp;";
                        }
                        else
                        {
                            echo '<a href="index.php?pagina='.$pag.'">'.$pag.'</a>&nbsp;&nbsp;&nbsp;';
                        }
                    }
                }
                ?>
                </div>
            </div>
        </div>

    </div>
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>BonAppetit 2017</p>
                <p>Nicole Schmidt (nicole.schmidt94@gmail.com)</p>
                <p>Joaquín Musanti (joakomusanti@gmail.com)</p>
            </div>
        </div>
    </footer>

</body>

</html>