<?php if(!isset($_SESSION)) {
    session_start();

    if (!isset($_SESSION["Admin"]))
    {
        echo '<script type="text/javascript">
                        window.location = "index.php"
                    </script>';

        return;
    }
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bon AppetitYA -- Cargar Comida</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="js/jquery.waypoints.min.js"></script>

    <!-- Magnific -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>
    <!-- Isotope & imagesLoaded -->
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <!-- GSAP  -->
    <script src="js/TweenLite.min.js"></script>
    <script src="js/CSSPlugin.min.js"></script>
    <script src="js/EasePack.min.js"></script>

    <!-- MAIN JS -->
    <script src="js/main.js"></script>

    <script type="text/javascript">


        $(document).ready(function () {
            $('#logOut').click(function()
            {
                var submit = "Salir";
                $.ajax({
                    type: "post",
                    url: "login.php",
                    data: {submit: submit},
                    success: function(){
                        window.location = "index.php";
                    }
                });
            });
        })
    </script>
</head>

<body>
<nav class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-left">
            <li><a href="index.php" class="link-logo"><img class="img-responsive imagen-logo" src="images/LOGO.jpg" alt="HOME"></a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="navbar-btn" href="contacto.php">Ayuda y Contacto</a> </li>
            <li><a class="navbar-btn" href="nosotros.php">Nosotros</a></li>
            <?php

            if (isset($_SESSION["Admin"]))
            {
                echo '<li><a class="navbar-btn" href="listarComidas.php">Listar Comidas</a></li>';
                echo '<li><a class="navbar-btn" href="cargarComida.php">Cargar Comida</a></li>';
            }
            if (isset($_SESSION["Usuario"]))
            {
                ?>
                <li><a class="navbar-btn" href="editarUsuario.php"><?php echo $_SESSION["Usuario"]["usuario"] ?></a></li>
                <li><a class="navbar-btn" href="#" id="logOut"><span class="fa fa-sign-out" aria-hidden="true"></span>Salir</a></li>
                <?php
            }
            else
            {
                ?>
                <li><a href="cargarUsuario.php" class="navbar-btn">Registrarse</a> </li>
                <li><a href="formularioLogin.php" class="navbar-btn"> <span class="fa fa-sign-in" aria-hidden="true"></span>Entrar</a></li>
                <?php
            } ?>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <form method="post" enctype="multipart/form-data" action="registrarComida.php">
                <div class="form-group">
                    <label for="txtNombre">Nombre:</label>
                    <input type="text" class="form-control" name="txtNombre" id="txtNombre" required>
                </div>
                <div class="form-group">
                    <label for="txtDescripcion">Descripción:</label>
                    <textarea  class="form-control" name="txtDescripcion" id="txtDescripcion" required></textarea>
                </div>
                <div class="form-group">
                    <label for="txtRestaurante">Nombre del restaurante:</label>
                    <input type="text" class="form-control" name="txtRestaurante" id="txtRestaurante" required>
                </div>
                <div class="form-group">
                    <label for="txtDir">Dirección del restaurante:</label>
                    <input type="text" class="form-control" name="txtDir" id="txtDir" required>
                </div>
                <div class="form-group">
                    <label for="precio">Precio:</label>
                    <input type="number" class="form-control" name="precio" id="precio" required>
                </div>
                <div class="form-group">
                    <label for="fotoPortada">Imagen de portada:</label>
                    <input type="file" id="fotoPortada" class="form-control" accept=".jpg" name="fotoPortada" required />
                </div>
                <input type="submit" class="btn btn-success" id="btnEnviar" value="Enviar">
            </form>
        </div>
    </div>
</div>

<footer>
    <div class="row">
        <div class="col-lg-12">
            <p>BonAppetit 2017</p>
            <p>Nicole Schmidt (nicole.schmidt94@gmail.com)</p>
            <p>Joaquín Musanti (joakomusanti@gmail.com)</p>
        </div>
    </div>
</footer>

</body>

</html>