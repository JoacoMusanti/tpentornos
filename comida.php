<?php
session_start();


    $id = $_GET["id"];

    include("conexion.inc");
    include("funcionesAuxiliares.php");

    $query = "SELECT comidas.descripcion as descripcionComida, comidas.direccionImagePortada, comidas.direccionRestaurante, comidas.evaluacion as evaluacionComida, comidas.id as idComida, comidas.nombre, comidas.precio, comidas.restaurante, comentarios.descripcion as descripcionComentario, comentarios.evaluacion, comentarios.fecha, comentarios.id as idComentario, comentarios.id_comida, comentarios.id_usuario, usuarios.usuario, comentarios.fecha 
              FROM comidas LEFT JOIN comentarios ON comidas.id = comentarios.id_comida LEFT JOIN usuarios ON usuarios.id = comentarios.id_usuario WHERE comidas.id = '$id'";
    $resultado = mysqli_query($link, $query) or die (mysqli_error($link));

    if (mysqli_num_rows($resultado) == 0)
    {
        echo '<script type="text/javascript">
                            window.location = "error404.php"
                        </script>';

        return;
    }

    $comida = mysqli_fetch_assoc($resultado);

    mysqli_close($link);
?>

<?php

function obtenerEstrellas($puntaje)
{
    $actualEstrellas = "";
    $estrellaLlena = "fa-star";
    $estrellaVacia = "fa-star-o";
    $estrellas = "<div class=\"star-rating\"><div class=\"star-rating__wrap\"><label class=\"star-readonly fa *TIPO-ESTRELLA-5* fa-lg\" title=\"5 out of 5 stars\"></label><label class=\"star-readonly fa *TIPO-ESTRELLA-4* fa-lg\" title=\"4 out of 5 stars\"></label><label class=\"star-readonly fa *TIPO-ESTRELLA-3* fa-lg\" title=\"3 out of 5 stars\"></label><label class=\"star-readonly fa *TIPO-ESTRELLA-2* fa-lg\" title=\"2 out of 5 stars\"></label><label class=\"star-readonly fa *TIPO-ESTRELLA-1* fa-lg\" title=\"1 out of 5 stars\"></label></div></div>";

    if ($puntaje == 5)
    {
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaLlena, $estrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaLlena, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaLlena, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaLlena, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
    }
    elseif ($puntaje == 4)
    {
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaLlena, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaLlena, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaLlena, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
    }
    elseif ($puntaje == 3)
    {
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaVacia, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaLlena, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaLlena, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
    }
    elseif ($puntaje == 2)
    {
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaVacia, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaVacia, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaLlena, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
    }
    elseif ($puntaje == 1)
    {
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaVacia, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaVacia, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaVacia, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
    }
    else
    {
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaVacia, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaVacia, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaVacia, $actualEstrellas);
        $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaVacia, $actualEstrellas);
    }

    return $actualEstrellas;
}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bon AppetitYA -- Comida</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="js/jquery.waypoints.min.js"></script>

    <!-- Magnific -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>
    <!-- Isotope & imagesLoaded -->
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <!-- GSAP  -->
    <script src="js/TweenLite.min.js"></script>
    <script src="js/CSSPlugin.min.js"></script>
    <script src="js/EasePack.min.js"></script>

    <!-- MAIN JS -->
    <script src="js/main.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $.ajax({
                data: {ejecutar: 'obtenerItemsCarro'},
                method: "post",
                url: "funcionesAuxiliares.php",
                success: function (respuesta) {
                    $("#cuerpoCarrito").html(respuesta);
                }
            });

            $.ajax({
                data: {ejecutar: 'calcularTotalCarro'},
                method: "post",
                url: "funcionesAuxiliares.php",
                success: function (respuesta) {
                    $("#precio").html(respuesta);
                }
            });

            $.ajax({
                data: {ejecutar: 'obtenerCantidadItemsCarro'},
                method: "post",
                url: "funcionesAuxiliares.php",
                success: function (respuesta) {
                    if (respuesta != 0)
                    {
                        $("#btnComprar").attr("disabled", false);
                    }
                    else
                    {
                        $("#btnComprar").attr("disabled", true);
                    }
                }
            });

            $("#cantidad").change(function () {
                if ($("#cantidad").val() < 1)
                {
                    $("#cantidad").val(1);
                }
            });

            $("#carrito").on("change", ".elemento-carrito", function () {
                var elemento = {id: $(this).attr("name"), cantidad: $(this).val()};

                if ($(this).val() > 0)
                {
                    $.ajax({
                        type: "post",
                        url: "actualizarCarro.php",
                        data: {pedido: elemento},
                        success: function(rta){
                            $("#precio").html(rta);
                        }
                    });
                }
                else
                {
                    $(this).val(1);
                }
            });

            $("#carrito").on("click", ".btnEliminar", function () {
                var id = $(this).attr("name");

                $.ajax({
                    data: { id: id },
                    method: "post",
                    url: "eliminarDelCarro.php",
                    success: function (respuesta) {
                        $.ajax({
                            data: {ejecutar: 'obtenerItemsCarro'},
                            method: "post",
                            url: "funcionesAuxiliares.php",
                            success: function (respuesta) {
                                $("#cuerpoCarrito").html(respuesta);
                            }
                        });

                        $.ajax({
                            data: {ejecutar: 'calcularTotalCarro'},
                            method: "post",
                            url: "funcionesAuxiliares.php",
                            success: function (respuesta) {
                                $("#precio").html(respuesta);
                            }
                        });

                        $.ajax({
                            data: {ejecutar: 'obtenerCantidadItemsCarro'},
                            method: "post",
                            url: "funcionesAuxiliares.php",
                            success: function (rta) {
                                if (rta != 0)
                                {
                                    $("#btnComprar").attr("disabled", false);
                                }
                                else
                                {
                                    $("#btnComprar").attr("disabled", true);
                                }
                            }
                        });
                    }
                });
            });

            $("#btnAgregarAlCarro").click(function () {
                var ID = $("#btnAgregarAlCarro").data("name");
                var CANT = $("#cantidad").val();

                $.ajax({
                    data: {id: ID, cantidad: CANT},
                    method: "post",
                    url: "agregarAlCarro.php",
                    success: function (respuesta) {
                        $("#cuerpoCarrito").html(respuesta);
                        $.ajax({
                            data: {ejecutar: 'calcularTotalCarro'},
                            method: "post",
                            url: "funcionesAuxiliares.php",
                            success: function (respuesta) {
                                $("#precio").html(respuesta);
                            }
                        });

                        $("#btnComprar").attr("disabled",false);
                    }
                });
            });

            $('#logOut').click(function()
            {
                var submit = "Salir";
                $.ajax({
                    type: "post",
                    url: "login.php",
                    data: {submit: submit},
                    success: function(){
                        window.location = "index.php";
                    }
                });
            });
        })
    </script>
</head>

<body>
<nav class="navbar navbar-default" >
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-left">
            <li><a href="index.php" class="link-logo"><img class="img-responsive imagen-logo" src="images/LOGO.jpg" alt="HOME"></a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="navbar-btn" href="contacto.php">Ayuda y Contacto</a> </li>
            <li><a class="navbar-btn" href="nosotros.php">Nosotros</a></li>
            <?php

            if (isset($_SESSION["Admin"]))
            {
                echo '<li><a class="navbar-btn" href="listarComidas.php">Listar Comidas</a></li>';
                echo '<li><a class="navbar-btn" href="cargarComida.php">Cargar Comida</a></li>';
            }
            if (isset($_SESSION["Usuario"]))
            {
                ?>
                <li><a class="navbar-btn" href="editarUsuario.php"><?php echo $_SESSION["Usuario"]["usuario"] ?></a></li>
                <li><a class="navbar-btn" href="#" id="logOut"><span class="fa fa-sign-out" aria-hidden="true"></span>Salir</a></li>
                <?php
            }
            else
            {
                ?>
                <li><a href="cargarUsuario.php" class="navbar-btn">Registrarse</a> </li>
                <li><a href="formularioLogin.php" class="navbar-btn"> <span class="fa fa-sign-in" aria-hidden="true"></span>Entrar</a></li>
                <?php
            } ?>
        </ul>
    </div>
</nav>
<div id="carrito" class="col-sm-3">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-info">
                <div id="cuerpoCarrito" class="panel-body">


                </div>
                <div class="panel-footer">
                    <div class="row text-center">
                        <div class="col-sm-9">
                            <h4 class="text-right">Total <strong>$<span id="precio"></span> </strong></h4>
                        </div>
                        <div class="col-sm-3">
                            <?php
                            if (!isset($_SESSION["Usuario"]))
                            {
                                echo '<p>Debe loguearse para realizar una compra</p>';
                            }
                            else
                            {
                                ?>
                                <form action="confirmarPedido.php" method="post">
                                    <input id="btnComprar" type="submit" value="Comprar" class="btn btn-success btn-block">
                                </form>
                                <?php
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid">

    <div class="row">
        <div class="col-sm-6">

            <div class="thumbnail">
                <img class="img-responsive" src="<?php echo $comida["direccionImagePortada"] ?>" alt="imagen de la comida">
                <div class="caption-full">
                    <strong class="pull-right"><?php echo '$'.$comida["precio"] ?></strong>
                    <strong><?php echo $comida["nombre"] ?></strong>
                    <p><?php echo $comida["descripcionComida"] ?></p>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="center-block">
                                <label for="cantidad">Indique la cantidad en el campo junto al botón</label>
                                <div class="input-group">
                            <span class="input-group-btn">
                                <a class="btn btn-default" id="btnAgregarAlCarro" data-name="<?php echo $comida["idComida"] ?>">Agregar al carrito</a>
                            </span>
                                    <input type="number" id="cantidad" value="1" min="1" class="form-control form-control-chico">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="ratings">
                    <p class="pull-right"><?php echo mysqli_num_rows($resultado) ?> comentarios</p>
                    <br>
                        <?php
                            echo obtenerEstrellas(round($comida["evaluacionComida"]))
                        ?>
                </div>
            </div>

            <div class="well">

                <?php
                if (isset($_SESSION["Usuario"])) {
                    include("conexion.inc");

                    $idCom = $comida["idComida"];
                    $idUsu = $_SESSION["Usuario"]['id'];

                    $query = "SELECT * FROM pedidos INNER JOIN comida_pedido ON pedidos.id = comida_pedido.id_pedido WHERE comida_pedido.id_comida = '$idCom' AND pedidos.id_usuario = '$idUsu'";
                    $resultadisimo = mysqli_query($link, $query) or die (mysqli_error($link));

                    mysqli_close($link);

                    if (mysqli_num_rows($resultadisimo) != 0)
                    {
                        ?>
                        <div class="text-right">
                            <a class="btn btn-success" data-toggle="modal" href="#" data-target="#modalComentario">Dejar un comentario</a>
                        </div>
                        <?php
                    }
                    else
                    {
                        ?>
                        <div class="text-right">
                            <p>Debe estar logueado y haber comprado este producto para dejar un comentario</p>
                        </div>
                        <?php
                    }
                }
                else
                {
                    ?>
                    <div class="text-right">
                        <p>Debe estar logueado y haber comprado este producto para dejar un comentario</p>
                    </div>
                <?php
                }
                ?>


                <?php

                mysqli_data_seek($resultado, 0);

                while ($row = mysqli_fetch_assoc($resultado))
                {
                    ?>
                    <hr>

                    <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                        <?php
                            echo obtenerEstrellas($row["evaluacion"]);
                            echo '<strong>'.$row["usuario"].'</strong><br>';
                            echo $row["descripcionComentario"];
                        ?>
                        </fieldset>
                    </div>
                </div>
                <?php
                }
                ?>


                <hr>

            </div>

        </div>



    </div>

</div>
<!-- /.container -->

<div class="container">

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>BonAppetit 2017</p>
                <p>Nicole Schmidt (nicole.schmidt94@gmail.com)</p>
                <p>Joaquín Musanti (joakomusanti@gmail.com)</p>
            </div>
        </div>
    </footer>

</div>


<div id="modalComentario" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form class="form-horizontal" method="POST" action="agregarComentario.php">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Dejá un comentario</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <label for="txtComentario" class="control-label">Comentario</label>
                            <textarea id="txtComentario" name="txtComentario" class="form-control" required></textarea>
                        </div>
                    </div>
                    <input type="hidden" id="idComida" name="idComida" value="<?php echo $comida["idComida"] ?>">
                    <div class="form-group">
                        <div class="star-rating">
                            <div class="star-rating__wrap">
                                <fieldset>
                                <input class="star-rating__input" id="star-rating-5a" type="radio" name="rating" value="5">
                                <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-5a" title="5 out of 5 stars">5</label>
                                <input class="star-rating__input" id="star-rating-4a" type="radio" name="rating" value="4">
                                <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-4a" title="4 out of 5 stars">4</label>
                                <input class="star-rating__input" id="star-rating-3a" type="radio" name="rating" value="3">
                                <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-3a" title="3 out of 5 stars">3</label>
                                <input class="star-rating__input" id="star-rating-2a" type="radio" name="rating" value="2">
                                <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-2a" title="2 out of 5 stars">2</label>
                                <input class="star-rating__input" id="star-rating-1a" type="radio" name="rating" value="1">
                                <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-1a" title="1 out of 5 stars">1</label>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="submit" class="btn btn-success" name="submit" value="Enviar">
                </div>
            </div>
        </form>
    </div>
</div>

</body>

</html>


















