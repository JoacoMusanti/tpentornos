<?php
    if (!isset($_SESSION))
    {
        session_start();
    }

    if(!isset($_SESSION['Usuario']) || !isset($_FILES["fotoPortada"]) || !isset($_POST["txtNombre"]) || !isset($_POST["txtDescripcion"]) || !isset($_POST["txtRestaurante"]) || !isset($_POST["txtDir"]) || !isset($_POST["precio"])){
        echo '<script type="text/javascript">
                        window.location = "index.php"
                    </script>';
        return;
    }

    include("conexion.inc");
    $nombre = $_POST["txtNombre"];
    $descripcion = $_POST["txtDescripcion"];
    $restaurante = $_POST["txtRestaurante"];
    $dirRestaurante = $_POST["txtDir"];
    $portada = $_FILES["fotoPortada"]["tmp_name"];
    $precio = $_POST["precio"];

    $nombre = mysqli_real_escape_string($link, $nombre);
    $descripcion = mysqli_real_escape_string($link, $descripcion);
    $restaurante = mysqli_real_escape_string($link, $restaurante);
    $dirRestaurante = mysqli_real_escape_string($link, $dirRestaurante);

    $directorio = "images/";
    $nombreArchivo = $directorio . basename(tempnam($directorio, 'port')) . ".jpg";
    move_uploaded_file($portada, $nombreArchivo);

    // grabamos la publicacion
    $query = "INSERT INTO comidas (nombre, descripcion, evaluacion, restaurante, precio, direccionImagePortada, direccionRestaurante, baja) values ('$nombre', '$descripcion', null, '$restaurante', '$precio', '$nombreArchivo', '$dirRestaurante', false)";
    mysqli_query($link, $query) or die (mysqli_error($link));

    mysqli_close($link);

    echo '<script type="text/javascript">
                        window.location = "listarComidas.php"
                    </script>';
?>
