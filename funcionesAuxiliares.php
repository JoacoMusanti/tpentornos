<?php
    if (isset($_POST['ejecutar'])) {
        if ($_POST['ejecutar'] == 'obtenerItemsCarro') {
            echo obtenerItemsCarro();
        }
        if ($_POST['ejecutar'] == 'calcularTotalCarro') {
            echo calcularTotalCarro();
        }
        if ($_POST['ejecutar'] == 'agregarTarjeta') {
            echo agregarTarjeta();
        }
        if ($_POST['ejecutar'] == 'obtenerCantidadItemsCarro')
        {
            echo obtenerCantidadItemsCarro();
        }
    }

    return;
?>

<?php
    function obtenerCantidadItemsCarro()
    {
        if (!isset($_SESSION))
        {
            session_start();
        }

        if (!isset($_SESSION["Carro"]) || count($_SESSION["Carro"]) == 0)
        {
            echo 0;
        }
        else
        {
            echo 1;
        }
    }

    function agregarTarjeta()
    {
        include("conexion.inc");

        $numero = $_POST["numero"];
        $codigo = $_POST["codigo"];
        $marca = $_POST["marca"];
        $idUsuario = $_POST["idUsuario"];

        $query = "INSERT INTO tarjetas (numero, codSeguridad, marca, id_usuario) VALUES ('$numero', '$codigo', '$marca', '$idUsuario')";
        mysqli_query($link, $query) or die (mysqli_error($link));
        $id = mysqli_insert_id($link);

        mysqli_close($link);

        echo $id;
    }

    function calcularTotalCarro()
    {
        if(!isset($_SESSION)) {
            session_start();
        }

        include("conexion.inc");

        if (isset($_SESSION["Carro"]) && count($_SESSION["Carro"]) > 0)
        {
            $carro = $_SESSION["Carro"];
        }
        else
        {
            echo "0";
            return null;
        }

        $ides = array();

        for ($i = 0; $i < count($carro); $i++)
        {
            array_push($ides, $carro[$i]['id']);
        }

        $query = "SELECT * FROM comidas WHERE id IN (".implode(',', $ides).")";
        $resultado = mysqli_query($link, $query) or die (mysqli_error($link));
        $total = 0;

        while ($row = mysqli_fetch_assoc($resultado))
        {
            for ($i = 0; $i < count($carro); $i++)
            {
                if ($row['id'] == $carro[$i]['id'])
                {
                    $total += $row['precio'] * $carro[$i]['cantidad'];
                }
            }
        }

        mysqli_close($link);

        echo $total;
    }

    function obtenerItemsCarro()
    {
        if(!isset($_SESSION)) {
            session_start();
        }

        include("conexion.inc");

        if (isset($_SESSION["Carro"]) && count($_SESSION["Carro"]) > 0)
        {
            $carro = $_SESSION["Carro"];
        }
        else
        {
            echo "No hay elementos en el carrito";
            return null;
        }

        $html = '<div class="row" name="*ID*">
                                    <div class="col-sm-8">
                                        <h4 class="product-name"><strong>*NOMBRE*</strong></h4><h4><small>*DESCRIPCION*</small></h4>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="text-right">
                                            <h6><strong>$*PRECIO*<span class="text-muted">x</span></strong></h6>
                                        </div>
                                        <div class="">
                                            <input name="*ID*" type="number" class="form-control input-sm elemento-carrito" min="1" value="*CANTIDAD*">
                                        </div>
                                        <div class="">
                                            <a href="#" name="*ID*" class="btnEliminar">
                                                <button type="button" class="btn btn-link btn-xs" >
                                                    <span class="glyphicon glyphicon-trash"> </span>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>';
        $actual = "";
        $devolucion = "";

        $ides = array();

        for ($i = 0; $i < count($carro); $i++)
        {
            array_push($ides, $carro[$i]['id']);
        }



        $query = "SELECT * FROM comidas WHERE id IN (".implode(',', $ides).")";
        $resultado = mysqli_query($link, $query) or die (mysqli_error($link));

        while ($row = mysqli_fetch_assoc($resultado))
        {
            $actual = str_replace("*ID*", $row['id'], $html);
            $actual = str_replace("*DESCRIPCION*", $row['descripcion'], $actual);
            $actual = str_replace("*PRECIO*", $row['precio'], $actual);
            $actual = str_replace("*NOMBRE*", $row['nombre'], $actual);
            for ($i = 0; $i < count($carro); $i++)
            {
                if ($carro[$i]['id'] == $row['id'])
                {
                    $actual = str_replace("*CANTIDAD*", $carro[$i]['cantidad'], $actual);
                }
            }

            $devolucion = $devolucion.$actual;
        }

        mysqli_close($link);

        echo $devolucion;
    }

?>