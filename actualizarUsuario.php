<?php

if(!isset($_SESSION))
{
    session_start();
}

if (!isset($_SESSION["Usuario"]))
{
    echo '<script type="text/javascript">
                        window.location = "index.php"
                    </script>';
    return;
}

if (!isset($_POST["txtNombre"]) || !isset($_POST["txtEmail"]) || !isset($_POST["txtPassword1"]) || !isset($_POST["txtPassword2"]) || !isset($_POST["txtTelefono"]) || !isset($_POST["txtDireccion"]) || !isset($_POST["txtApellido"]))
{
    echo '<script type="text/javascript">
                        window.location = "editarUsuario.php"
                    </script>';
    return;
}

$email = $_POST["txtEmail"];
$pass1 = $_POST["txtPassword1"];
$pass2 = $_POST["txtPassword2"];
$telefono = $_POST["txtTelefono"];
$direccion = $_POST["txtDireccion"];
$nombre = $_POST["txtNombre"];
$apellido = $_POST["txtApellido"];
$id = $_SESSION["Usuario"]["id"];


if (strcmp($pass1, $pass2) != 0)
{
    echo '<script type="text/javascript">
                        window.location = "editarUsuario.php"
                    </script>';
    return;
}

include ("conexion.inc");

$query = "UPDATE usuarios SET password = '$pass1', rol = 2, telefono = '$telefono', direccion = '$direccion', nombre = '$nombre', apellido = '$apellido', email = '$email' WHERE id = '$id'";
mysqli_query($link, $query) or die (mysqli_error($link));

$query = "SELECT * FROM usuarios WHERE id = '$id'";
$resultado = mysqli_query($link, $query) or die (mysqli_error($link));

$_SESSION["Usuario"] = mysqli_fetch_assoc($resultado);

mysqli_close($link);

echo '<script type="text/javascript">
                            window.location = "index.php"
                        </script>';
return;
?>