<?php
    if(!isset($_SESSION)) {
        session_start();
    }

    $id = $_POST["id"];

    if (isset($_SESSION["Carro"]) && count($_SESSION["Carro"]) > 0)
    {
        $carro = $_SESSION["Carro"];
    }
    else
    {
        $carro = array();
        $_SESSION["Carro"] = $carro;
    }


    for ($i = 0; $i < count($carro); $i++)
    {
        if ($carro[$i]['id'] == $id)
        {
            unset($carro[$i]);
            $carro = array_values($carro);
            break;
        }
    }

    $_SESSION["Carro"] = $carro;

    include("funcionesAuxiliares.php");

    return;
?>