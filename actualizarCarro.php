<?php
    if (!isset($_SESSION))
    {
        session_start();
    }

    if (isset($_SESSION["Carro"]) && count($_SESSION["Carro"]) > 0)
    {
        $carro = $_SESSION["Carro"];
    }

    $pedido = $_POST["pedido"];

    $comidaActual = $pedido;

    for ($i = 0; $i < count($carro); $i++)
    {
        if ($carro[$i]['id'] == $comidaActual['id'])
        {
            $carro[$i]['cantidad'] = $comidaActual['cantidad'];
        }
    }

    $_SESSION["Carro"] = $carro;

    include("funcionesAuxiliares.php");

    echo calcularTotalCarro();
?>