<?php if(!isset($_SESSION)) {
    session_start();

    if (!isset($_SESSION["Usuario"]))
    {
        echo '<script type="text/javascript">
                        window.location = "index.php"
                    </script>';

        return;
    }
}



?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bon AppetitYa -- Confirmar Pedido</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="js/jquery.waypoints.min.js"></script>

    <!-- Magnific -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>
    <!-- Isotope & imagesLoaded -->
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <!-- GSAP  -->
    <script src="js/TweenLite.min.js"></script>
    <script src="js/CSSPlugin.min.js"></script>
    <script src="js/EasePack.min.js"></script>

    <!-- MAIN JS -->
    <script src="js/main.js"></script>

    <script type="text/javascript">


        $(document).ready(function () {
            $("#frmTarjeta").submit(function (e) {

                $.ajax({
                    data: { ejecutar: 'agregarTarjeta', numero: $("#txtNumero").val(), codigo: $("#txtCodigo").val(), marca: $("#marca").val(), idUsuario: $("#idUsuario").val() },
                    method: "post",
                    url: "funcionesAuxiliares.php",
                    success: function (respuesta) {
                        $("#modalTarjeta").modal('hide');
                        $("#selectTarjeta").append($('<option>', {
                            value: respuesta,
                            text: $("#txtNumero").val() + " " + $("#marca").val()
                        }));
                    }
                });

                e.preventDefault();

            });

            $('#logOut').click(function()
            {
                var submit = "Salir";
                $.ajax({
                    type: "post",
                    url: "login.php",
                    data: {submit: submit},
                    success: function(){
                        window.location = "index.php";
                    }
                });
            });
        })
    </script>
</head>

<body>
<nav class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-left">
            <li><a href="index.php" class="link-logo"><img class="img-responsive imagen-logo" src="images/LOGO.jpg" alt="HOME"></a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="navbar-btn" href="contacto.php">Ayuda y Contacto</a> </li>
            <li><a class="navbar-btn" href="nosotros.php">Nosotros</a></li>
            <?php

            if (isset($_SESSION["Admin"]))
            {
                echo '<li><a class="navbar-btn" href="listarComidas.php">Listar Comidas</a></li>';
                echo '<li><a class="navbar-btn" href="cargarComida.php">Cargar Comida</a></li>';
            }
            if (isset($_SESSION["Usuario"]))
            {
                ?>
                <li><a class="navbar-btn" href="editarUsuario.php"><?php echo $_SESSION["Usuario"]["usuario"] ?></a></li>
                <li><a class="navbar-btn" href="#" id="logOut"><span class="fa fa-sign-out" aria-hidden="true"></span>Salir</a></li>
                <?php
            }
            else
            {
                ?>
                <li><a href="cargarUsuario.php" class="navbar-btn">Registrarse</a> </li>
                <li><a href="formularioLogin.php" class="navbar-btn"> <span class="fa fa-sign-in" aria-hidden="true"></span>Entrar</a></li>
                <?php
            } ?>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="col-sm-12">
        <form action="registrarPedido.php" method="post">
            <div class="form-group">
                <label for="txtNombre">Nombre:</label>
                <input id="txtNombre" type="text" class="form-control" disabled="disabled" value="<?php echo $_SESSION["Usuario"]["nombre"] ?>">
            </div>
            <div class="form-group">
                <label for="txtApellido">Apellido:</label>
                <input id="txtApellido" type="text" class="form-control" disabled="disabled" value="<?php echo $_SESSION["Usuario"]["apellido"] ?>">
            </div>
            <div class="form-group">
                <label for="txtTelefono">Teléfono:</label>
                <input id="txtTelefono" type="text" class="form-control" disabled="disabled" value="<?php echo $_SESSION["Usuario"]["telefono"] ?>">
            </div>
            <div class="form-group">
                <label for="txtDireccion">Dirección:</label>
                <input id="txtDireccion" type="text" class="form-control" value="<?php echo $_SESSION["Usuario"]["direccion"] ?>">
            </div>
            <br>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="selectTarjeta">Tarjeta de crédito:</label>
                        <select id="selectTarjeta" class="form-control" required>
                            <?php
                            include("conexion.inc");

                            $idUsuario = $_SESSION["Usuario"]["id"];

                            $query = "SELECT tarjetas.id as idTarjeta, tarjetas.numero as numTarjeta, marca FROM usuarios INNER JOIN tarjetas ON usuarios.id = tarjetas.id_usuario WHERE usuarios.id = '$idUsuario'";
                            $resTarjeta = mysqli_query($link, $query) or die (mysqli_error($link));

                            $usuarioTarjetas = mysqli_fetch_assoc($resTarjeta);

                            $idTar = $usuarioTarjetas['idTarjeta'];
                            $numTar = $usuarioTarjetas['numTarjeta'];
                            $marca = $usuarioTarjetas['marca'];

                            echo '<option value="'.$idTar.'">'.$numTar.' '.$marca.'</option>';

                            while ($usuarioTarjetas = mysqli_fetch_assoc($resTarjeta))
                            {
                                $idTar = $usuarioTarjetas['idTarjeta'];
                                $numTar = $usuarioTarjetas['numTarjeta'];
                                $marca = $usuarioTarjetas['marca'];

                                echo '<option value="'.$idTar.'">'.$numTar.' '.$marca.'</option>';
                            }

                            mysqli_close($link);

                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <br>
                        <a data-toggle="modal" href="#" data-target="#modalTarjeta" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-plus"></span></a>
                    </div>

                </div>
            </div>

        <br>
            <br>
            <table class="table table-responsive">
                <thead>
                    <tr>
                    <th>Nombre</th>
                    <th>Restaurant</th>
                    <th class="text-center">Cantidad</th>
                    <th class="text-center">Precio unitario</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $carro = $_SESSION["Carro"];

                    include("conexion.inc");

                    for ($i = 0; $i < count($carro); $i++)
                    {
                        $id = $carro[$i]['id'];
                        $query = "SELECT nombre, precio, restaurante FROM comidas WHERE id='$id'";
                        $resultado = mysqli_query($link, $query) or die (mysqli_error($link));

                        $row = mysqli_fetch_assoc($resultado);

                        echo '<tr>';
                        echo '<td>'.$row["nombre"].'</td>';
                        echo '<td>'.$row["restaurante"].'</td>';
                        echo '<td class="text-center">'.$carro[$i]['cantidad'].'</td>';
                        echo '<td class="text-center">'.$row["precio"].'</td>';
                        echo '</tr>';
                    }

                    mysqli_close($link);
                ?>
                </tbody>
            </table>
            <input type="submit" value="Confirmar Pedido" class="btn btn-success">
        </form>
    </div>
</div>


<div id="modalTarjeta" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form id="frmTarjeta" class="form-horizontal" method="POST">

            <div class="modal-content container-fluid">
                <div class="modal-header row">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregá una tarjeta</h4>
                </div>
                <div class="modal-body row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="form-group">
                            <label for="txtNumero" class="control-label">Número</label>
                            <input type="number" max="9999999999999999" id="txtNumero" name="txtNumero" class="form-control" required placeholder="Sin espacios ni guiones">
                        </div>
                        <input type="hidden" id="idUsuario" name="idUsuario" value="<?php echo $_SESSION["Usuario"]['id'] ?>">
                        <div class="form-group">
                            <label for="txtCodigo">Código de seguridad</label>
                            <input type="number" max="999" id="txtCodigo" name="txtCodigo" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="marca">Marca</label>
                            <select id="marca" class="form-control">
                                <option value="VISA">VISA</option>
                                <option value="MASTERCARD">Master Card</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer row">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="submit" class="btn btn-success" name="submit" value="Enviar">
                </div>
            </div>
        </form>
    </div>
</div>

<footer>
    <div class="row">
        <div class="col-lg-12">
            <p>BonAppetit 2017</p>
            <p>Nicole Schmidt (nicole.schmidt94@gmail.com)</p>
            <p>Joaquín Musanti (joakomusanti@gmail.com)</p>
        </div>
    </div>
</footer>

</body>

</html>