<?php
    if(!isset($_SESSION)) {
        session_start();
    }

    $id = $_POST["id"];
    $cantidad = $_POST["cantidad"];

    if (isset($_SESSION["Carro"]) && count($_SESSION["Carro"]) > 0)
    {
        $carro = $_SESSION["Carro"];
    }
    else
    {
        $carro = array();
        $_SESSION["Carro"] = $carro;
    }

    $comidaActual = array(
        'id' => $id,
        'cantidad' => $cantidad
    );

    $encontrado = false;

    for ($i = 0; $i < count($carro); $i++)
    {
        if ($carro[$i]['id'] == $comidaActual['id'])
        {
            $carro[$i]['cantidad'] += $comidaActual['cantidad'];
            $encontrado = true;
        }
    }

    if (!$encontrado)
    {
        array_push($carro, $comidaActual);
    }

    $_SESSION["Carro"] = $carro;

    include("funcionesAuxiliares.php");

    echo obtenerItemsCarro();
?>