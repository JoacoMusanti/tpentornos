<?php
    include("conexion.inc");

    $busqueda = $_POST["busqueda"];

    $query = "SELECT * FROM comidas WHERE nombre LIKE '%$busqueda%' AND baja <> true ORDER BY evaluacion ASC LIMIT 10";
    $resultado = mysqli_query($link, $query) or die (mysqli_error($link));

    $html = "<a href=\"comida.php?id=*ID*\"><div class=\"card card-small\"><img src=\"*DIRECCION*\" class=\"img-responsive thumbnail thumbnail-sin-padding\" alt=\"Avatar\"><div class=\"container-card\"><div class='texto-corto'> <h4><b>*NOMBRE*</b></h4></div>*ESTRELLAS*<p class='texto-corto'>*RESTAURANTE*</p><h4 class=\"pull-right\">$*PRECIO*</h4></div></div></a>";
    $estrellas = "<div class=\"star-rating\"><div class=\"star-rating__wrap\"><label class=\"star-readonly fa *TIPO-ESTRELLA-5* fa-lg\" for=\"star-rating-5\" title=\"5 out of 5 stars\"></label><label class=\"star-readonly fa *TIPO-ESTRELLA-4* fa-lg\" for=\"star-rating-4\" title=\"4 out of 5 stars\"></label><label class=\"star-readonly fa *TIPO-ESTRELLA-3* fa-lg\" for=\"star-rating-3\" title=\"3 out of 5 stars\"></label><label class=\"star-readonly fa *TIPO-ESTRELLA-2* fa-lg\" for=\"star-rating-2\" title=\"2 out of 5 stars\"></label><label class=\"star-readonly fa *TIPO-ESTRELLA-1* fa-lg\" for=\"star-rating-1\" title=\"1 out of 5 stars\"></label></div></div>";
    $actual = "";
    $actualEstrellas = "";
    $devolucion = "";
    $estrellaLlena = "fa-star";
    $estrellaVacia = "fa-star-o";

    while ($row = mysqli_fetch_array($resultado))
    {
        $puntaje = round($row['evaluacion']);

        $actual = str_replace("*ID*", $row['id'], $html);
        $actual = str_replace("*DIRECCION*", $row['direccionImagePortada'], $actual);
        $actual = str_replace("*NOMBRE*", $row['nombre'], $actual);
        $actual = str_replace("*PRECIO*", $row['precio'], $actual);
        $actual = str_replace("*RESTAURANTE*", $row['restaurante'], $actual);

        if ($puntaje == 5)
        {
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaLlena, $estrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaLlena, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaLlena, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaLlena, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
        }
        elseif ($puntaje == 4)
        {
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaLlena, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaLlena, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaLlena, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
        }
        elseif ($puntaje == 3)
        {
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaVacia, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaLlena, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaLlena, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
        }
        elseif ($puntaje == 2)
        {
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaVacia, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaVacia, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaLlena, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
        }
        elseif ($puntaje == 1)
        {
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaVacia, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaVacia, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaVacia, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaLlena, $actualEstrellas);
        }
        else
        {
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-5*", $estrellaVacia, $estrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-4*", $estrellaVacia, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-3*", $estrellaVacia, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-2*", $estrellaVacia, $actualEstrellas);
            $actualEstrellas = str_replace("*TIPO-ESTRELLA-1*", $estrellaVacia, $actualEstrellas);
        }

        $actual = str_replace("*ESTRELLAS*", $actualEstrellas, $actual);


        $devolucion = $devolucion . $actual;
    }

    mysqli_close($link);

    echo $devolucion;
?>