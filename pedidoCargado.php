<?php if(!isset($_SESSION)) {
    session_start();
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bon AppetitYa &mdash; Pedido Cargado</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="js/jquery.waypoints.min.js"></script>

    <!-- Magnific -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>
    <!-- Isotope & imagesLoaded -->
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <!-- GSAP  -->
    <script src="js/TweenLite.min.js"></script>
    <script src="js/CSSPlugin.min.js"></script>
    <script src="js/EasePack.min.js"></script>

    <!-- MAIN JS -->
    <script src="js/main.js"></script>
</head>

<body>
<nav class="navbar navbar-default" >
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-left">
            <li><a href="index.php" class="link-logo"><img class="img-responsive imagen-logo" src="images/LOGO.jpg" alt="HOME"></a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="navbar-btn" href="contacto.php">Ayuda y Contacto</a> </li>
            <li><a class="navbar-btn" href="nosotros.php">Nosotros</a></li>

            <?php

            if (isset($_SESSION["Admin"]))
            {
                echo '<li><a class="navbar-btn" href="listarComidas.php">Listar Comidas</a></li>';
                echo '<li><a class="navbar-btn" href="cargarComida.php">Cargar Comida</a></li>';
            }
            if (isset($_SESSION["Usuario"]))
            {
                ?>
                <li><a class="navbar-btn" href="editarUsuario.php"><?php echo $_SESSION["Usuario"]["usuario"] ?></a></li>
                <li><a class="navbar-btn" href="#" id="logOut"><i class="fa fa-sign-out" aria-hidden="true"></i>Salir</a></li>
                <?php
            }
            else
            {
                ?>
                <li><a href="cargarUsuario.php" class="navbar-btn">Registrarse</a> </li>
                <li><a href="formularioLogin.php" class="navbar-btn"> <i class="fa fa-sign-in" aria-hidden="true"></i>Entrar</a></li>
                <?php
            } ?>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <h2>Pedido cargado exitosamente, haga click <a href="index.php">aquí</a> para volver a la home</h2>
        </div>
    </div>
</div>
<footer>
    <div class="row">
        <div class="col-lg-12">
            <p>BonAppetit 2017</p>
            <p>Nicole Schmidt (nicole.schmidt94@gmail.com)</p>
            <p>Joaquín Musanti (joakomusanti@gmail.com)</p>
        </div>
    </div>
</footer>

</body>

</html>